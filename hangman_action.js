

var watchwords = JSON.parse('{"1":"Bez pracy nie ma kołaczy",\
"2":"Darowanemu koniowi w zęby się nie zagląda",\
"3":"Fortuna kołem się toczy",\
"4":"Nie chwal dnia przed zachodem słońca",\
"5":"Lepszy wróbel w garści niż gołąb na dachu",\
"6":"Apetyt rośnie w miarę jedzenia",\
"7":"Co ma wisieć nie utonie",\
"8":"Dzieci i ryby głosu nie mają",\
"9":"Grosz do grosza a będzie kokosza",\
"10":"Łaska pańska na pstrym koniu jeździ"}');

var gameEnd = false;
var min = 1;
var max = Object.keys(watchwords).length;
var random = min + Math.floor((max - min) * Math.random());
var originWatchword = watchwords[random];
var hiddenWatchword = "";
var alphabet = "AĄBCĆDEĘFGHIJKLŁMNŃOÓPQRSŚTUVWXYZŻŹ";
var usedChars = "";
var attempts = 0;


String.prototype.replaceAt = function(index, chars){
	return this.substr(0, index) + chars + this.substr(index + chars.length);
}

function showWatchword(){
	originWatchword = originWatchword.toUpperCase();
	var watchwordSpan = "<span id=\"watchword\">" + originWatchword + "</span>"
	document.getElementById("watchword").innerText = originWatchword;
}

function showHiddenWatchword(){
	hiddenWatchword = hiddenWatchword.toUpperCase();
	document.getElementById("watchword").innerText = hiddenWatchword;
}

function startHiddenWatchword(){
	originWatchword = originWatchword.toUpperCase();
	for (i=0; i<originWatchword.length; i++) {
		if (originWatchword.charAt(i) == " "){
			hiddenWatchword += " "			
		} else {
			hiddenWatchword += "-"
		}
	}
	document.getElementById("watchword").innerHTML = hiddenWatchword;
}

function printButtons(){
	var buttons = "";
	for (i=0; i<35; i++){
		if (i%7 == 0){
			buttons +="<div style=\"clear: both;\"></div>";
		} 		
		buttons += "<span class=\"Button\" id=\"buttonId" +
		 i + "\" onclick=\"checkOnClick(" + i + ")\"  >" + alphabet.charAt(i) + "</span>";
	}
	document.getElementById("alphabet").innerHTML = buttons;
}

function printPhotoStage(number){
	var stage = "<img src=\"stages_hangman_phot/s"+ number +".jpg\" />";
	document.getElementById("hangmanGraphics").innerHTML = stage;
}

function start(){
	startHiddenWatchword();
	printButtons();
	printPhotoStage(attempts);
}

function checkOnClick(id){
	var buttonId = "buttonId" + id;
	var button = document.getElementById(buttonId);
	var char = button.innerText;
	
	if (attempts < 9 && usedChars.search(char) < 0 && !gameEnd) {
		let found = false;
		usedChars += char;

		for (i=0; i<originWatchword.length; i++){
			if (char == originWatchword.charAt(i)){
				found = true;
				hiddenWatchword = hiddenWatchword.replaceAt(i, char);
				showHiddenWatchword();
			}
		}

		if (found) {
			buttonUsedStyle(button, true);
		} else {
			buttonUsedStyle(button, false);
		}

		if (hiddenWatchword.search("-") < 0) {
			gameEnd = true;
			var endPopup = document.getElementById("endPopup");
			endPopup.style.display = "flex";
			document.getElementById("endPopup").innerText = "You Win!"; 
		}

		if (!found && attempts < 9){
			attempts++;
			printPhotoStage(attempts);
		}
		if (attempts >= 9){
			gameEnd = true;
			var endPopup = document.getElementById("endPopup");
			endPopup.style.display = "flex";
			document.getElementById("endPopup").innerText = "You Lost!"; 
		}
	}
}

function buttonUsedStyle(button, properly){
	if (properly) {
		button.style.background = 'rgba(11, 156, 49, 0.8)';
	} else {
		button.style.background = 'rgba(231, 76, 60, 0.8)';
	}
	button.style.cursor = "auto";
}